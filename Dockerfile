# PHP-Node based image
FROM node:16-alpine

LABEL maintainer="Rohan Sakhale <rs@saiashirwad.com>"

RUN apk add --update --no-cache \
    zip \
    curl \
    php81 \
    php81-bcmath \
    php81-common \
    php81-curl \
    php81-dom \
    php81-gd \
    php81-exif \
    php81-fileinfo \
    php81-iconv \
    php81-phar \
    php81-gmp \
    php81-mysqlnd \
    php81-mbstring \
    php81-openssl \
    php81-simplexml \
    php81-sqlite3 \
    php81-tokenizer \
    php81-xml \
    php81-xmlwriter \
    php81-zip

RUN ln -s /usr/bin/php81 /usr/bin/php


RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --install-dir=/usr/bin --filename=composer \
    && php -r "unlink('composer-setup.php');"

CMD ["php", "composer", "node", "npm"]